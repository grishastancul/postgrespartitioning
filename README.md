# README #

Application to support the partitioning demo

### Run
```
mvn clean compile
mvn spring-boot:run
```

Then go to http://localhost:8086/showMeasurements

## Database

#### Doku
https://www.postgresql.org/docs/9.5/static/ddl-partitioning.html

#### Check version

`select version();`

#### Create database

#### Create Table 
```
CREATE SEQUENCE hibernate_sequence
   INCREMENT 1
   START 1;

CREATE TABLE measurement (
    id         		int not null DEFAULT nextval('hibernate_sequence'::regclass),
    logdate         date not null,
    peaktemp        int,
    unitsales       int
);

INSERT INTO measurement(logdate, peaktemp, unitsales)
    VALUES (now(), 27, 100);

SELECT id, logdate, peaktemp, unitsales
  FROM measurement;
```

### Create partitions

```
CREATE TABLE measurement_y2018m05 ( ) INHERITS (measurement);
CREATE TABLE measurement_y2018m06 ( ) INHERITS (measurement);
CREATE TABLE measurement_y2018m07 ( ) INHERITS (measurement);
```

Experiment with data
```
INSERT INTO measurement(logdate, peaktemp, unitsales)
    VALUES (now(), 27, 101);

SELECT id, logdate, peaktemp, unitsales, tableoid::regclass
  FROM measurement;
  
SELECT id, logdate, peaktemp, unitsales, tableoid::regclass
  FROM measurement_y2018m06;


INSERT INTO measurement_y2018m05(logdate, peaktemp, unitsales)
    VALUES (now(), 27, 102);

SELECT id, logdate, peaktemp, unitsales, tableoid::regclass
  FROM measurement_y2018m05;

SELECT id, logdate, peaktemp, unitsales, tableoid::regclass
  FROM measurement;
  
```

Create checks
```
DROP TABLE IF EXISTS measurement_y2018m05;
CREATE TABLE measurement_y2018m05 (
    CHECK ( logdate >= DATE '2018-05-01' AND logdate < DATE '2018-06-01' )
) INHERITS (measurement);
DROP TABLE IF EXISTS measurement_y2018m06;
CREATE TABLE measurement_y2018m06 (
    CHECK ( logdate >= DATE '2018-06-01' AND logdate < DATE '2018-07-01' )
) INHERITS (measurement);
DROP TABLE IF EXISTS measurement_y2018m07;
CREATE TABLE measurement_y2018m07 (
    CHECK ( logdate >= DATE '2018-07-01' AND logdate < DATE '2018-08-01' )
) INHERITS (measurement);
```

Experiment with data
```
INSERT INTO measurement_y2018m05(logdate, peaktemp, unitsales)
    VALUES (now(), 27, 103);

INSERT INTO measurement_y2018m06(logdate, peaktemp, unitsales)
    VALUES (now(), 27, 103);

SELECT id, logdate, peaktemp, unitsales, tableoid::regclass
  FROM measurement;
  
```

Create redirect trigger

```
CREATE OR REPLACE FUNCTION measurement_insert_trigger()
RETURNS TRIGGER AS $$
BEGIN
    IF ( NEW.logdate >= DATE '20018-05-01' AND
         NEW.logdate < DATE '2018-06-01' ) THEN
        INSERT INTO measurement_y2018m05 VALUES (NEW.*);
    ELSIF ( NEW.logdate >= DATE '2018-06-01' AND
            NEW.logdate < DATE '2018-07-01' ) THEN
        INSERT INTO measurement_y2018m06 VALUES (NEW.*);
    ELSIF ( NEW.logdate >= DATE '2018-07-01' AND
            NEW.logdate < DATE '2018-08-01' ) THEN
        INSERT INTO measurement_y2018m07 VALUES (NEW.*);
    ELSE
        RAISE EXCEPTION 'Date out of range.  Fix the measurement_insert_trigger() function!';
    END IF;
    RETURN NULL;
END;
$$
LANGUAGE plpgsql;
```

```
CREATE TRIGGER insert_measurement_trigger
    BEFORE INSERT ON measurement
    FOR EACH ROW EXECUTE PROCEDURE measurement_insert_trigger();
```

Experiment with data
```
INSERT INTO measurement(logdate, peaktemp, unitsales)
    VALUES (now(), 27, 104);

SELECT id, logdate, peaktemp, unitsales, tableoid::regclass
  FROM measurement;
  
SELECT id, logdate, peaktemp, unitsales, tableoid::regclass
  FROM measurement_y2018m06;


COPY  measurement (logdate, peaktemp, unitsales) FROM '/home/gstancul/work/ideaProjects/testing/postgrespartitioning/src/test/resources/measurements_for_copy.txt' (DELIMITER ';');
SELECT id, logdate, peaktemp, unitsales, tableoid::regclass
  FROM measurement;
  
```

