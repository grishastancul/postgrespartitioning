package gst.partitions.jpa.controller;

import java.util.List;

import gst.partitions.jpa.service.*;
import gst.partitions.jpa.bean.Measurement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class HtmlController {

    @Autowired
    MeasurementService measurementService;

    @GetMapping("/showMeasurements")
    public String findMeasurements(Model model) {

        List<Measurement> measurements = measurementService.findAll();

        model.addAttribute("measurements", measurements);

        return "showMeasurements";
    }
}