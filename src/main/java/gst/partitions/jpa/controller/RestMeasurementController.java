package gst.partitions.jpa.controller;

import gst.partitions.jpa.bean.Measurement;
import gst.partitions.jpa.service.MeasurementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RestMeasurementController {

    @Autowired
    MeasurementService measurementService;

    @RequestMapping(path = "/api/measurements", method = RequestMethod.POST)
    public void addMeasurement(@RequestBody Measurement measurement) {
        measurementService.save(measurement);
    }

    @RequestMapping(path = "/api/measurements/native", method = RequestMethod.POST)
    public void addMeasurementNative(@RequestBody Measurement measurement) {
        measurementService.insertNative(measurement);
    }

    @RequestMapping(path = "/api/measurements", method = RequestMethod.GET)
    public List<Measurement> getMeasurements() {
        return measurementService.findAll();
    }

}
