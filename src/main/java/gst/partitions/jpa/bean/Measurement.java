package gst.partitions.jpa.bean;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "measurement")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Measurement {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Date logdate;
    private int peaktemp;
    private int unitsales;
}
