package gst.partitions.jpa.service;

import gst.partitions.jpa.bean.Measurement;
import gst.partitions.jpa.repository.MeasurementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class MeasurementService {

    @Autowired
    private MeasurementRepository repository;

    @PersistenceContext
    private EntityManager entityManager;

    public List<Measurement> findAll() {
        return (List<Measurement>) repository.findAll();
    }

    public void save(Measurement measurement) {
        repository.save(measurement);
    }

    @Transactional
    public void insertNative(Measurement measurement) {
        Query nativeQuery = entityManager.createNativeQuery("INSERT INTO measurement(logdate, peaktemp, unitsales) VALUES (?, ?, ?)");
        nativeQuery.setParameter(1, measurement.getLogdate());
        nativeQuery.setParameter(2, measurement.getPeaktemp());
        nativeQuery.setParameter(3, measurement.getUnitsales());
        nativeQuery.executeUpdate();
    }

}